FROM php:8.0

RUN apt-get update

RUN apt-get install -qq git curl \
libmcrypt-dev \ 
libjpeg-dev \ 
libpng-dev

RUN apt-get clean

RUN docker-php-ext-install mcrypt pdo_mysql zip

# Agregando composer
RUN curl --silent --show-error "https://getcomposer.org/installer" | php -- --install-dir=/usr/local/bin --filename=composer

RUN composer global require "laravel/envoy=~1.0"